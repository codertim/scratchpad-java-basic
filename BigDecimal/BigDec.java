

import java.math.*;
     
     
// class to experiment with BigDecimal class
public class BigDec {
     
	private static void experimentWithBigDecimals() {
		BigDecimal bigDec1 = BigDecimal.valueOf(753.42);
		System.out.println("simple big decimal from .valueOf= " + bigDec1);
     
		BigDecimal bigDec2 = new BigDecimal("753.42");
		System.out.println("simple big decimal from String constructor = " + bigDec2);
     
		BigDecimal bigDec3a = new BigDecimal("10.333");
		BigDecimal bigDec3b = new BigDecimal("7.25");
		BigDecimal bigDec3c = bigDec3a.subtract(bigDec3b);
		System.out.println("big decimal from subtraction = " + bigDec3c);
	}


	public static void main(String [] args) {
		System.out.println("Starting ...");
 
		experimentWithBigDecimals();

		System.out.println("Done.");
	}
}


