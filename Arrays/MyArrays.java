
import java.util.Arrays;


public class MyArrays {

	public static void doArraySort() {
		System.out.println("##### doArraySort");

		int[] intArray = {1, 5, 3, 9, 2};
		System.out.println("  Array before sort - " + Arrays.toString(intArray));

		Arrays.sort(intArray);
		System.out.println("  Array after sort - " + Arrays.toString(intArray));
	}


	public static void doArrayCopy() {
		System.out.println("##### doArrayCopy");

		System.out.println("  Using Arrays.copyOf ... ");
		int[] intArray = {3, 4, 5, 6};
		int[] copiedIntArray = Arrays.copyOf(intArray, intArray.length * 2);
		System.out.println("    copyied array via Arrays.copyOf = " + Arrays.toString(copiedIntArray));

		System.out.println("  Using System.arraycopy ... ");
		int[] array1 = {1, 2, 3, 4, 5};
		int[] array2 = {11, 12, 13, 14, 15};
		System.out.println("    array1 = " + Arrays.toString(array1));
		System.out.println("    array2 = " + Arrays.toString(array2));
		System.arraycopy(array1, 1, array2, 3, 2);
		System.out.println("    copied array via System.arraycopy = " + Arrays.toString(array2));
	}


	public static void main(String [] args) {
		System.out.println("Starting ...");

		doArraySort();
		doArrayCopy();

		System.out.println("Done.");
	}
}

